__author__ = 'rakesh.varma'
import math
import threading, time
from functools import wraps

def timefn(fn):
    @wraps(fn)
    def measure_time(*args, **kwargs):
        t1 = time.time()
        result = fn(*args, **kwargs)
        t2 = time.time()
        print ("@timefn:" + fn.func_name + " took " + str(t2 - t1) + " seconds")
        return result
    return measure_time

def is_prime(num):
    for j in range(2,int(math.sqrt(num)+1)):
        if (num % j) == 0:
            return False
    return True

def prime(nth):
    i = 0
    num = 2
    while i < nth:
        if is_prime(num):
            i += 1
            if i == nth:
                print('The ' + str(nth) + 'th prime number is: ' + str(num))
        num += 1

@timefn
def singleThreadtest():
    for count in range(12):
        prime(10000)


@timefn
def multiThreadtest():
    threads = []
    for count in range(12):
        t = threading.Thread(target = prime, args=[10000])
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

if __name__ == "__main__":
    singleThreadtest()
    multiThreadtest()