This program demonstrates multi-threading in python on a CPU bound problem (determine nth prime number). We would see that because of the limitations due to GIL, the use of multi-threading degrades the performance.

To run this, python testprime.py
