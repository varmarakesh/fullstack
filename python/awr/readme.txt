
1. Make sure Oracle client exists, tnsnames.ora file exists with the database that needs to be tested in question.
2. Install cx_Oracle, ensure other libraries used in generate_awr.py are setup.
3. I saved the AWR data to my local directory path, /Users/rakesh.varma/Downloads/. Change it accordingly.
4. Run the program 
	python generate_awr.py DLINKD

This has been tested on Mac, OEL using python 2.7
