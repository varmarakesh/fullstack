__author__ = 'rakesh.varma'
import cx_Oracle
import csv
import threading, time, os, sys
from functools import wraps

pool = cx_Oracle.SessionPool('SYSTEM', 'MANAGER', sys.argv[1], 1,20,1, threaded = True)

def timefn(fn):
    @wraps(fn)
    def measure_time(*args, **kwargs):
        t1 = time.time()
        result = fn(*args, **kwargs)
        t2 = time.time()
        print ("@timefn:" + fn.func_name + " took " + str(t2 - t1) + " seconds")
        return result
    return measure_time

def getOracleConnection():
    connection = pool.acquire()
    return connection


def getDirectoryPath():
        return '/Users/rakesh.varma/Downloads/'

awr_directory_path = getDirectoryPath()

@timefn
def get_snapshots():
    conn = getOracleConnection()
    q_awr_snap = ''' SELECT distinct SNAP_ID, to_char(END_INTERVAL_TIME, 'hh24:mi_dd_mon_yy') FROM DBA_HIST_SNAPSHOT WHERE TRUNC(END_INTERVAL_TIME) = TRUNC(SYSDATE) ORDER BY SNAP_ID'''
    cursor = conn.cursor()
    cursor.execute(q_awr_snap)
    snaps = []
    for c in cursor:
        snaps.append({'snap_id' : c[0], 'snap_end_time' : c[1]})
    cursor.close()
    return snaps


def save_awr(snap):
    q_awr = ''' SELECT
                     D.NAME,
                     SS.SNAP_ID,
                     SS.INSTANCE_NUMBER,
                     SS.SQL_ID,
                     ST.SQL_TEXT,
                     MODULE,
                     ACTION,
                     PARSING_SCHEMA_NAME,
                     FETCHES_TOTAL,
                     SORTS_TOTAL,
                     EXECUTIONS_TOTAL,
                     LOADS_TOTAL,
                     PARSE_CALLS_TOTAL,
                     DISK_READS_TOTAL,
                     BUFFER_GETS_TOTAL,
                     ROWS_PROCESSED_TOTAL,
                     CPU_TIME_TOTAL,
                     IOWAIT_TOTAL,
                     ELAPSED_TIME_TOTAL
                    FROM
                     DBA_HIST_SQLSTAT SS,
                     DBA_HIST_SNAPSHOT SP,
                     DBA_HIST_SQLTEXT ST,
                     V$DATABASE D
                    WHERE
                     SS.SNAP_ID = SP.SNAP_ID AND
                     SS.DBID = SP.DBID AND
                     SS.DBID = D.DBID AND
                     SS.INSTANCE_NUMBER = SP.INSTANCE_NUMBER AND
                     SS.DBID = ST.DBID AND
                     SS.SQL_ID = ST.SQL_ID AND
                     SS.SNAP_ID = :SNAP_ID'''
    path = getDirectoryPath() + "AWR_{1}_{2}_{3}.csv".format(awr_directory_path,sys.argv[1],snap['snap_id'], snap['snap_end_time'])


    conn = getOracleConnection()
    cursor = conn.cursor()
    cursor.execute(q_awr, [snap['snap_id']])
    with open(path, "wb") as csv_file:
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow([i[0] for i in cursor.description]) # write headers
        csv_writer.writerows(cursor)
    cursor.close()

@timefn
def generate_awr_multithreads():
    snapshots = get_snapshots()
    threads = []
    for requestcount in range(snapshots.__len__()):
        t = threading.Thread(target = save_awr, args=[snapshots[requestcount]])
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

@timefn
def generate_awr_singlethread():
    snapshots = get_snapshots()
    for requestcount in range(snapshots.__len__()):
        save_awr(snap = snapshots[requestcount])


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Invalid arguments, only 1 argument (tnsname) is allowed, exiting.....'
        exit()
    generate_awr_multithreads()
    generate_awr_singlethread()
