package xsd.tests;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class XmlFactory {
	public String xsdFile;
	
	public XmlFactory(String xsd) {
		this.xsdFile = xsd;
	}
	
	public String readXmlFile( String filename ) throws IOException {
		  
	      FileReader xmlFile = new FileReader(filename);
	      StringBuffer xmlString = new StringBuffer();
	      int readCharacter = 0;
	      try {
	         while( ( readCharacter = xmlFile.read() ) > -1 ) {
	            xmlString.append( (char)readCharacter );
	         }
	      }
	      catch( IOException e ) {
	         // TODO: put exception message here
	      }
	      finally {
	         try {
	            xmlFile.close();
	         }
	         catch( IOException e ) {
	            // TODO: put exception message here
	         }
	      }
	      xmlFile.close();
	      return xmlString.toString();
	   }
	
	public boolean XsdValidate(String inputXml)throws SAXException, IOException {
		// build the schema
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		File schemaFile = new File(xsdFile);
		Schema schema = factory.newSchema(schemaFile);
		Validator validator = schema.newValidator();

		// create a source from a string
		Source source = new StreamSource(new StringReader(inputXml));

		// check input
		boolean isValid = true;
		try {
		validator.validate(source);
		} catch (SAXException e) {
			System.err.println(e.getMessage());
			isValid = false;
		}
		return isValid;
	}
	
	public String getUpdatedXml(String fileContents, String xpath_expression, String value) throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException, SAXException, IOException, ParserConfigurationException {
		Document doc = DocumentBuilderFactory.newInstance()
		        .newDocumentBuilder().parse(new InputSource( new StringReader(fileContents)));

		    // locate the node(s)
		    XPath xpath = XPathFactory.newInstance().newXPath();
		    NodeList nodes = (NodeList)xpath.evaluate
		        (xpath_expression, doc, XPathConstants.NODESET);

		    // make the change
		    for (int idx = 0; idx < nodes.getLength(); idx++) {
		      nodes.item(idx).setTextContent(value);
		    }
		 
		    // save the result
		    StringWriter writer = new StringWriter();
		    Transformer xformer = TransformerFactory.newInstance().newTransformer();
		    xformer.transform
		        (new DOMSource(doc), new javax.xml.transform.stream.StreamResult(writer));
		    return writer.toString();
	}
	
	public String removeNode(String fileContents, String xpath_expression) throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException, SAXException, IOException, ParserConfigurationException {
		Document doc = DocumentBuilderFactory.newInstance()
		        .newDocumentBuilder().parse(new InputSource( new StringReader(fileContents)));

		    // locate the node(s)
		    XPath xpath = XPathFactory.newInstance().newXPath();
		    NodeList nodes = (NodeList)xpath.evaluate
		        (xpath_expression, doc, XPathConstants.NODESET);

		    // make the change
		    for (int idx = 0; idx < nodes.getLength(); idx++) {
		      nodes.item(idx).getParentNode().removeChild(nodes.item(idx));
		    }
		 
		    // save the result
		    StringWriter writer = new StringWriter();
		    Transformer xformer = TransformerFactory.newInstance().newTransformer();
		    xformer.transform
		        (new DOMSource(doc), new javax.xml.transform.stream.StreamResult(writer));
		    return writer.toString();
	}
	
	
}
