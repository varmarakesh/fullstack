package xsd.tests;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.testng.Assert;
import org.testng.annotations.*;

// TestNG suite of tests.
public class SimpleTest {
	
	String xsd;
	XmlFactory xmlFactory;
	String fileContents;
	
	@BeforeSuite
	public void Initialize() throws IOException {
		xsd = "/Users/rakesh.varma/Documents/workspace1/xsdtests/src/xsd/tests/shiporder.xsd";
		xmlFactory = new XmlFactory(xsd);
		fileContents = xmlFactory.readXmlFile("/Users/rakesh.varma/Documents/workspace1/xsdtests/src/xsd/tests/shiporder.xml");
	}
	
	//Another test in a group.
	@Test
	public void test_xsd_validate() throws SAXException, IOException, ParserConfigurationException {
		Assert.assertTrue(xmlFactory.XsdValidate(fileContents));
	}
	
	@Test
	public void test_price_string() throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException, SAXException, IOException, ParserConfigurationException {
		String updatedFileContents = xmlFactory.getUpdatedXml(fileContents, "/shiporder/item[1]/price", "John Paul");
		Assert.assertFalse(xmlFactory.XsdValidate(updatedFileContents));
	}
	
	@Test
	public void test_price_number() throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException, SAXException, IOException, ParserConfigurationException {
		String updatedFileContents = xmlFactory.getUpdatedXml(fileContents, "/shiporder/item[1]/price", "5.0");
		Assert.assertTrue(xmlFactory.XsdValidate(updatedFileContents));
	}
	
	@Test
	public void test_price_node_deletion() throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException, SAXException, IOException, ParserConfigurationException {
		String updatedFileContents = xmlFactory.removeNode(fileContents, "/shiporder/item[1]/price");
		Assert.assertTrue(xmlFactory.XsdValidate(updatedFileContents));
	}
	
	@Test
	public void test_title_number() throws XPathExpressionException, TransformerFactoryConfigurationError, TransformerException, SAXException, IOException, ParserConfigurationException{
		String updatedFileContents = xmlFactory.getUpdatedXml(fileContents, "/shiporder/item/title", "3");
		System.out.println(updatedFileContents);
		Assert.assertFalse(xmlFactory.XsdValidate(updatedFileContents));
	}
}

