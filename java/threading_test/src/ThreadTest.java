
class Prime {
	public Boolean is_prime(int num) {
		for(int i=2; i < (int) (Math.sqrt(num)+1); i++){
			if((num % i) == 0) {
				return false;
			}
		}
		return true;
	}
	
	public void prime(int nth){
		int i = 0;
		int num = 2;
		while (i < nth) {
			if(is_prime(num)) {
				i = i+1;
				if (i == nth) {
					System.out.printf("The %d th prime number is: %d \n", nth, num);
				}
			}
			num = num + 1;
		}
	}
}
class RunnableThread implements Runnable {

	Thread runner;
	public RunnableThread() {
	}
	public RunnableThread(String threadName) {
		runner = new Thread(this, threadName); // (1) Create a new thread.
		System.out.println(runner.getName());
		runner.start(); // (2) Start the thread.
	}
	
	public void run() {
		//Display info about this particular thread
		Prime p = new Prime();
		p.prime(10000);
		System.out.println(Thread.currentThread());
	}
}

public class ThreadTest {
	
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		int threadCount = 12;
		Thread[] threads = new Thread[threadCount];
		for(int i=0; i<threadCount; i++) {
			Thread thread = new Thread(new RunnableThread());
			threads[i] = thread;
			thread.start();
		}
		for(int i=0; i<threadCount; i++){
			threads[i].join();
		}
		long endTime = System.currentTimeMillis();
		System.out.printf("Multi - Thread Test took %d milliseconds \n", (endTime - startTime) );
		
		startTime = System.currentTimeMillis();
		for(int i=0; i<threadCount; i++){
			Prime p = new Prime();
			p.prime(10000);
		}
		endTime = System.currentTimeMillis();
		System.out.printf("Single - Thread Test took %d milliseconds. \n", (endTime - startTime) );
	}

}
